import 'package:flutter/material.dart';

import 'designer.dart';

class CompleteApp extends StatelessWidget{
  @override
  Widget build(BuildContext context) {

    final customAppBar = AppBar(
        leading: Icon(Icons.menu),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        toolbarHeight: 90,
        title: Text("Designers"),
        centerTitle: true,
        flexibleSpace: Container(
          decoration: const BoxDecoration(
              borderRadius: BorderRadius.only(bottomLeft: Radius.circular(20),bottomRight: Radius.circular(20)),
              gradient: LinearGradient(
                  colors: [Colors.blue,Colors.lightBlueAccent],
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter
              )
          ),
        ),
    );
    return Scaffold(
      appBar: customAppBar,
      body: ListView(
        children: [
          Designer("assets/img/perfil.jpg","Designer 1","2 years"),
          Designer("assets/img/perfil.jpg","Designer 2","3 years"),
          Designer("assets/img/perfil.jpg","Designer 3","6 months"),
          Designer("assets/img/perfil.jpg","Designer 1","2 years"),
          Designer("assets/img/perfil.jpg","Designer 2","3 years"),
          Designer("assets/img/perfil.jpg","Designer 3","6 months"),
          Designer("assets/img/perfil.jpg","Designer 1","2 years"),
          Designer("assets/img/perfil.jpg","Designer 2","3 years"),
          Designer("assets/img/perfil.jpg","Designer 3","6 months"),
          Designer("assets/img/perfil.jpg","Designer 1","2 years"),
          Designer("assets/img/perfil.jpg","Designer 2","3 years"),
          Designer("assets/img/perfil.jpg","Designer 3","6 months")
        ],
      ),
    );
  }

}