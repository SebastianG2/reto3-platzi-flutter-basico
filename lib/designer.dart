import 'package:flutter/material.dart';

class Designer extends StatelessWidget{
  String pathImage = "assets/img/perfil_egel.jpg";
  String name = 'Sebas Garcia';
  String detail = "1 review 5 photos";

  Designer(this.pathImage, this.name, this.detail, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build



    final userInfo = Container(
      margin: const EdgeInsets.only(
          left: 20.0
      ),
      child: Text(
        detail,
        textAlign: TextAlign.left,
        style: const TextStyle(
            fontFamily: "Lato",
            fontSize: 13.0,
            color: Color(0xFFa3a5a7)
        ),
      ),
    );

    final userName = Container(
      margin: const EdgeInsets.only(
          left: 20.0
      ),
      child: Text(
        name,
        textAlign: TextAlign.left,
        style: const TextStyle(
            fontFamily: "Lato",
            fontSize: 17.0
        ),
      ),
    );


    final photo = Container(
        width: 80.0,
        height: 80.0,
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            image: DecorationImage(
                fit: BoxFit.cover,
                image: AssetImage(pathImage)
            )
        )
    );

    final mailIcon = Container(
      margin: const EdgeInsets.only(
        top: 5.0,
      ),
      child: const Icon(Icons.mail),
    );

    return ListTile(
        title: userName,
        isThreeLine:
        true, //will fix the alignment if the subtitle text is too big
        subtitle: userInfo,
        leading: photo,
        trailing: mailIcon,
    );


  }

}